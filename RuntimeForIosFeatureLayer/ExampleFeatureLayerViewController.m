//
//  ExampleFeatureLayerViewController.m
//  RuntimeForIosFeatureLayer
//
//  Created by Esri Australia on 22/10/2014.
//  Copyright (c) 2014 Esri Australia. All rights reserved.
//

#import "ExampleFeatureLayerViewController.h"

@interface ExampleFeatureLayerViewController ()
@property AGSFeatureLayer *featureLayer;
@end

@implementation ExampleFeatureLayerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Bootstrap - this is not part of the example - start...
    NSURL* url = [NSURL URLWithString:@"http://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer"];
    AGSTiledMapServiceLayer *basemap = [AGSTiledMapServiceLayer tiledMapServiceLayerWithURL:url];
    [self.agsMapView addMapLayer:basemap withName:@"Basemap Tiled Layer"];
    AGSEnvelope *envelope = [AGSEnvelope envelopeWithXmin:-19839092.3042881 ymin:2145729.67991779 xmax:-7454985.14651674  ymax:11542624.9160411  spatialReference:self.agsMapView.spatialReference];
    [self.agsMapView zoomToEnvelope:envelope animated:NO];
    //Bootstrap - this is not part of the example - end.
    

    //Example Starts Here... add the feature layer to the agsMapView
    NSURL *featureLayerURL = [NSURL URLWithString:@"http://services.arcgis.com/P3ePLMYs2RVChkJx/ArcGIS/rest/services/USA_States_Generalized/FeatureServer/0"];
    self.featureLayer = [AGSFeatureLayer featureServiceLayerWithURL:featureLayerURL mode:AGSFeatureLayerModeOnDemand];
    [self.agsMapView addMapLayer:self.featureLayer withName:@"Feature Layer"];
    
    //default set the defintionExpression to off (not part of the example)
    [self.definitionExpressionUISwitch setOn:NO animated:NO];
}


- (IBAction)visibilityUISwitchAction:(UISwitch *)sender {
    //Example - toggle the visibility of the layer on the map
    [self.featureLayer setVisible:sender.isOn];
}

- (IBAction)definitionExpressionUISwitchAction:(UISwitch *)sender {
    //Example start - set a definitionExpression to turn some of the content within a layer on or off.
    if (sender.isOn){
        //set the definition expression to show states with square miles > 100000.0
        [self.featureLayer setDefinitionExpression:@"SQMI > 100000.0"];
        [self.featureLayer refresh];
    } else {
        //remove the definition expression
        [self.featureLayer setDefinitionExpression:@""];
        [self.featureLayer refresh];
    }
    //Example end.
    
    //supplimentary to the example, we'll just show the expression on screen for reference.
    [self.definitionExpressionLabel setText:self.featureLayer.definitionExpression];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
@end
