//
//  ExampleFeatureLayerViewController.h
//  RuntimeForIosFeatureLayer
//
//  Created by Esri Australia on 22/10/2014.
//  Copyright (c) 2014 Esri Australia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ArcGIS/ArcGIS.h>

@interface ExampleFeatureLayerViewController : UIViewController
@property (strong, nonatomic) IBOutlet AGSMapView *agsMapView;
@property (strong, nonatomic) IBOutlet UILabel *definitionExpressionLabel;

- (IBAction)visibilityUISwitchAction:(UISwitch *)sender;

@property (strong, nonatomic) IBOutlet UISwitch *definitionExpressionUISwitch;

- (IBAction)definitionExpressionUISwitchAction:(UISwitch *)sender;

@end
